
# Stack docker-compose pour Applications Symfony (pour Alpes Isère Habitat) 

Ce stack Docker à pour objectif de fournir un environnement capable d’exécuter des applications Php/MySql avec le Framework Symfony.
L'usage de ce stack est à destination essentiellement des phases de développement et (CI) d'Intégration Continue de Alpes Isère Habitat.

## Composants du stack
* MySQL (Version 5)
* phpMyAdmin (dernière version)
* Maildev
* Apache2
* Php 8.2

## Modules Additionnels
* Libicu-dev
* g++
* libpng-dev
* libxml2-dev
* libzip-dev
* libonig-dev
* composer
* intl
* acpu
* pdo
* pdo_mysql
* gd
* opcache
* zip
* calendar
* dom
* mbstring
* libldap2-dev
* ldap
* xsl
* wkthtmltopdf

## Installation
L'installation du stack ce réalise dans l'ordre suivant :
* Cloner ce projet Git sur votre machine
* Entrer dans le repertoire
* Créer le repertoire data
* Entrer dans le repetoire data
* Créer le repertoire de perdsistence WWW
* Retourner à la racine
* Faire un "build" pour construire l'image des containers Apache et php7.4.1

```bash
git clone https://gitlab.com/yoan.bernabeu/docker-symfony.git
cd docker-symfony
mkdir data
cd data
mkdir www
cd ..
docker-compose build
```

## Usage

Lancer le stack
```docker-compose
docker-compose up -d
```

Afficher la liste des conteneurs
```docker-compose
docker-compose ps
```

Acceder au shell du contenaire Apache/Php
```bash
docker exec -it -u root docker-symfony_php740_1 bash
```

Copier un projet Symfony (Depuis le shell du contenaire Apache/Php)
```bash
docker exec -it -u root docker-symfony_php740_1 bash
cd /var/www/
git clone http://adresse-du-git .
composer require symfony/apache-pack
composer install
```

Paramétrage DB dans le fichier .ENV
```.ENV
DATABASE_URL=mysql://root:@db:3306/nom_database
```

Si problèmes de droits (à faire dans le repertoire www)
```bash
chmod -R 777 *
```

Si "Error 500"
```bash
Il faut supprimer le .htacess à la racine du repertoire public
```

## Port Mapping
* MySql : 3307
* phpMyAdmin : 8080
* Maildev : 8002
* Apache : 9074

## Directory (Persistance des données)
* MySql : ./data/db/ 
* www : ./data/www/
* vhosts : ./vhosts/

## Mise à jour de l'image PHP 7.4 sur le Hub Docker
Editer le fichier Dockerfile, en ajoutant les modules et extentions manquantes.
Il suffit ensuite de faire un build de l'image, et la publier sur le hub docker en lui précisant un numéro de version (à adapter avec vos infos du Hub Docker ):
```bash
$ docker build -t alpesiserehabitat/symfonyaih .
$ docker tag {id du build} alpesiserehabitat/symfonyaih:{Version}
$ docker login
$ docker push alpesiserehabitat/symfonyaih:{Version}
```

## Auteurs
* Création initiale de Guillaume Ponty
* Modifications & adaptations de Yoan Bernabeu pour Alpes isère Habitat 

## Sources
* https://medium.com/@romaricp/the-perfect-kit-starter-for-a-symfony-4-project-with-docker-and-php-7-2-fda447b6bca1
* https://blog.dev-web.io/2019/02/10/utiliser-symfony-dans-docker/
* https://blog.silarhi.fr/image-docker-php-apache-parfaite/
* https://symfony.com/doc/4.0/setup/web_server_configuration.html
